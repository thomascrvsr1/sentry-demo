# Sentry demo

Liens de démo :
- [sentry](https://thomascrvsr.sentry.io/projects/sentry-demo-frontend/?project=4507305943760977&statsPeriod=14d)
- environments:
    - [staging 1.10](https://sentry-demo-thomascrvsr1-e60e8996192d7cb1523b56c6f3cbef8a52cdf2.gitlab.io/staging-1.10/)
    - [production 1.0](https://sentry-demo-thomascrvsr1-e60e8996192d7cb1523b56c6f3cbef8a52cdf2.gitlab.io/production-1.0/)
    - [production 1.9](https://sentry-demo-thomascrvsr1-e60e8996192d7cb1523b56c6f3cbef8a52cdf2.gitlab.io/production-1.9/)

