To demo:
- alerting on runtime exceptions
- source maps
- performance tracking ?
- HTTP errors when calling a backend
- Python SDK ?
- release management
- link with JIRA etc...
- different environments (prod, preprod, etc...)
- user info (email or ID)

TODO:
- have a proper backend
- deploy the thing on a server ?
- have several releases to demo ?
- configure the sourcemaps thingy
