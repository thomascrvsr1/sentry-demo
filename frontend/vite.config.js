import { sentryVitePlugin } from "@sentry/vite-plugin";
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

const environment = process.env.VITE_ENVIRONMENT || "local";
const version = process.env.VITE_APP_VERSION || "latest"
const fullVersion = [environment, version].join("-");

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react(), sentryVitePlugin({
        org: "thomascrvsr",
        project: "sentry-demo-frontend"
    })],
    build: {
        outDir: `dist/${fullVersion}`,
        sourcemap: true
    },
    base: `${fullVersion}/`
})