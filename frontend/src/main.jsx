import * as Sentry from '@sentry/react';

import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'

const config = {
    environment: import.meta.env.VITE_ENVIRONMENT || "local",
    version: import.meta.env.VITE_APP_VERSION || "latest"
};

Sentry.init({
    dsn: "https://dd54c3fa23736590435a00c2b605c15e@o4507301286969344.ingest.de.sentry.io/4507305943760977",
    integrations: [],
    environment: config.environment,
    release: config.version
});

const container = document.getElementById("root");
const root = ReactDOM.createRoot(container);

root.render(<App />);
